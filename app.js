var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var request = require('request');
var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
var async = require('async');
var net = require('net')
var fs = require('fs')

var client = new net.Socket()
require('mongoose-double')(mongoose);
var default_parameters = require('./src/config/default.parameters.js');
var sendTcp = require("./src/tcp/aAPI.js")
var SchemaTypes = mongoose.Schema.Types;

var figlet = require('figlet');
var moment = require('moment-timezone');

/*var routes = require('./routes/index');
 var users = require('./routes/users');*/

figlet('CEPS', function(err, data) {
    if (err) {
        console.log('CEPS...');
        console.dir(err);
        return;
    }
    console.log(data)
});
mongoose.connect('mongodb://localhost/ceps');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    console.log("Database Connected " + moment().tz("Asia/Kolkata").format().toString());
});

var db_schema = new mongoose.Schema({
    command: Number,
    device_id: String,
    voltage: SchemaTypes.Double,
    e1: SchemaTypes.Double,
    e2: SchemaTypes.Double,
    e3: SchemaTypes.Double,
    e4: SchemaTypes.Double,
    e5: SchemaTypes.Double,
    date_time: String
});

var error_schema = mongoose.Schema({
    error_string: String,
    date_time: String
});

var DB = mongoose.model("DB", db_schema);
var ERROR_DATA = mongoose.model("errorData", error_schema);

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


app.get('/adevices', function(req, res) {
    console.log("Query Recieved=>" + req.query + "Number:\t" + req.query.from)
    var message = String(req.query.message).split(" ");
    console.log("Message Length before IF=>" + message.length);
    if (message.length === 2) {


        var sample1 = message[1].replace(/[$#]/g, '');
        var array = sample1.split(',');
        console.log("Message Length=>" + message.length + "\tArray LEngth => " + array.length);
        if (parseFloat(array[2]) >= default_parameters.voltage && array.length === 8) {

            console.log("TOO HEAVY Voltage");
            async.parallel({
                one: function(callback) {
                    //var sms_url = "http://www.login.aonesms.com/sendurlcomma.aspx?user=20064619&pwd=6xuxkn&senderid=ABC&mobileno=8884060111,8074511783,9912701623&msgtext=Use the following verification code:127&smstype=0"; 
                    //var sms_url = "High Voltage alert. The earth pit 123 connected to device 123 at abc is reporting high voltage of 10 volts. This is above the threshold and needs your  attention."
                    var sms_url = "http://login.aonesms.com/sendurlcomma.aspx?user=20064619&pwd=6xuxkn&senderid=PAPAYA&mobileno=9912701623,9949285123,9849744264,8008012444&msgtext=High Voltage alert. The earth pit gshshgd connected to device gagagh at hshsh is reporting high voltage of " + array[2] + " volts. This is above the threshold and needs your attention&smstype=0"
                        // request(sms_url, function(error, response, body) {
                        //     if (!error && response.statusCode == 200) {
                        //         console.log("ERROR =>" + error + "\tResponse =>" + response.statusCode + "\tBody=>" + body); // Show the HTML for the Google homepage.
                        //         callback(null, "SMS SENT");
                        //     } else {
                        //         console.log("SMS NOT SENT" + "\t ERRRRRRRRR" + error)
                        //     }
                        // })
                },
                two: function(callback) {
                    var db_data = new DB({
                        command: array[0],
                        device_id: array[1],
                        voltage: array[2],
                        e1: array[3],
                        e2: array[4],
                        e3: array[5],
                        e4: array[6],
                        e5: array[7],
                        date_time: moment().tz("Asia/Kolkata").format().toString()
                    });
                    db_data.save(function(err, results) {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, results);
                        }
                    });
                },
                three: function(callback) {
                    sendTcp.sendTcp("$00,SIPL,254.00,00.00,00.00,00.00,00.00,00.00#")
                    callback(null, "OK TCP")
                }
            }, function(err, results) {
                console.log("ERROR=>" + err + "\tREsults=>" + results.three)
            });
        } else if (parseFloat(array[2]) <= default_parameters.voltage && array.length === 8) {
            var db_data = new DB({
                command: array[0],
                device_id: array[1],
                voltage: array[2],
                e1: array[3],
                e2: array[4],
                e3: array[5],
                e4: array[6],
                e5: array[7],
                date_time: moment().tz("Asia/Kolkata").format().toString()
            });
            db_data.save(function(err, results) {
                if (err) {
                    console.error(err);
                } else {
                    console.log(results)
                }
            });
        } else {
            var error_dataa = new ERROR_DATA({
                error_string: String(message),
                date_time: moment().tz("Asia/Kolkata").format().toString()
            });
            error_dataa.save(function(err, results) {
                console.log("ERROR=>" + err + "\tREsults=>" + results);
            });
        }
    } else {
        var error_data = new ERROR_DATA({
            error_string: String(message),
            date_time: moment().tz("Asia/Kolkata").format().toString()
        });
        error_data.save(function(err, results) {
            console.log("ERROR=>" + err + "\tREsults=>" + results)
        });
    }
    res.json({ "data": req.query });
});

app.get('/devices', function(req, res) {
    console.log("-----------------------")
    console.log("TIME -> " + moment().tz("Asia/Kolkata").format().toString())
    console.log("Query Recieved=>" + JSON.stringify(req.query) + "Number:\t" + req.query.from)
    console.log("-----------------------")
    var message = String(req.query.message).split(" ");
    //console.log("Message Length before IF=>" + message.length);
    if (message.length === 2) {


        var sample1 = message[1].replace(/[$#]/g, '');
        var array = sample1.split(',');
        //console.log("Message Length=>" + message.length + "\tArray LEngth => " + array.length);
        if (array.length === 8) {


            async.parallel({
                one: function(callback) {
                    for (var i = 3; i < 8; i++) {
                        if (parseFloat(array[i]) >= default_parameters.voltage) {
                            var earth_pit = i - 2
                            console.log("-----------------------")
                            console.log("TOO HEAVY Voltage" + "E->" + earth_pit + "\tVOLTS->" + array[i])
                            console.log("-----------------------")
                            var sms_url = "http://login.aonesms.com/sendurlcomma.aspx?user=20064619&pwd=6xuxkn&senderid=PAPAYA&mobileno=9912701623,9949285123,9849744264&msgtext=High Voltage alert. The earth pit CEP053 connected to device Earth pit " + earth_pit + " at RailTel is reporting high voltage of " + array[i] + " volts. This is above the threshold and needs your attention&smstype=0"
                            request(sms_url, function(error, response, body) {
                                if (!error && response.statusCode == 200) {
                                    //console.log("ERROR =>" + error + "\tResponse =>" + response.statusCode + "\tBody=>" + body); // Show the HTML for the Google homepage.
                                    //callback(null, "SMS SENT");
                                    console.log("SMS SENT")
                                } else {
                                    console.log("SMS NOT SENT" + "\t ERRRRRRRRR" + error)
                                }
                            })
                        }
                    }
                    callback(null, "SMS SENT")

                },
                two: function(callback) {
                    var db_data = new DB({
                        command: array[0],
                        device_id: array[1],
                        voltage: array[2],
                        e1: array[3],
                        e2: array[4],
                        e3: array[5],
                        e4: array[6],
                        e5: array[7],
                        date_time: moment().tz("Asia/Kolkata").format().toString()
                    });
                    db_data.save(function(err, results) {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, results);
                        }
                    });
                },
                three: function(callback) {
                    //sendTcp.sendTcp("$00,SIPL,254.00,00.00,00.00,00.00,00.00,00.00#")
                    client.connect(8081, '183.82.99.67', function() {
                        console.log('Connected')
                        client.write("$00,SIPL,254.00,00.00,00.00,00.00,00.00,00.00#")
                    })
                    callback(null, "OK TCP")

                }
            }, function(err, results) {
                console.log("-----------------------")
                console.log("ERROR=>" + err + "\tREsults OF ASYNC=>" + results.three)
                console.log("-----------------------")
            })
        }
        if (array.length !== 8) {
            var error_dataa = new ERROR_DATA({
                error_string: array,
                date_time: moment().tz("Asia/Kolkata").format().toString()
            });
            error_dataa.save(function(err, results) {
                console.log("-----------------------")
                console.log("ERROR=>" + err + "\t DATA SAVED ERROR LENGTH OF PACKET 8 =>" + results)
                console.log("-----------------------")
            });
        }
    } else {
        var error_data = new ERROR_DATA({
            error_string: array,
            date_time: moment().tz("Asia/Kolkata").format().toString()
        });
        error_data.save(function(err, results) {
            console.log("-----------------------")
            console.log("ERROR=>" + err + "\t DATA SAVED ERROR LENGTH OF PACKET 2 =>" + results)
            console.log("-----------------------")
        });
    }
    res.json({ "data": req.query });
});

app.get('/logs', (req, res) => {
    res.sendFile("/home/deploy/.pm2/logs/CEPS-CLONE-out-14.log")
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;